# Generated by Django 4.1.7 on 2023-08-28 15:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phototag', '0014_rasgo_altura_frente_rasgo_forma_cejas_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='foto',
            name='nube_puntos',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
    ]
