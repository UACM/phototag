# Generated by Django 4.1.7 on 2023-03-03 00:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('phototag', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='relacion',
            options={'verbose_name_plural': 'Relaciones familiares'},
        ),
        migrations.RemoveField(
            model_name='foto',
            name='base_nariz',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='color_ojos',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='comisura_labios',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='contorno_rostro',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='forma_ojos',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='forma_orejas',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='grosor_labios',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='linealidad_ojos',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='nacimiento_cabello',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='nariz_perfil',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='pilosidad_cejas',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='pilosidad_facial',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='pliegue_epicantico',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='proyeccion_labios',
        ),
        migrations.RemoveField(
            model_name='foto',
            name='tipo_cabello',
        ),
        migrations.CreateModel(
            name='Rasgo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('etapa', models.CharField(choices=[('n', 'niñez'), ('a', 'adolescencia'), ('A', 'adultez')], max_length=1, verbose_name='etapa de desarrollo')),
                ('nacimiento_cabello', models.PositiveSmallIntegerField(verbose_name='Nacimiento del cabello')),
                ('tipo_cabello', models.PositiveSmallIntegerField(verbose_name='Tipo de cabello')),
                ('contorno_rostro', models.PositiveSmallIntegerField(verbose_name='Contorno del rostro')),
                ('pilosidad_cejas', models.PositiveSmallIntegerField(verbose_name='Pilosidad de cejas')),
                ('linealidad_ojos', models.PositiveSmallIntegerField(verbose_name='Linealidad de los ojos')),
                ('forma_ojos', models.PositiveSmallIntegerField(verbose_name='Forma de los ojos')),
                ('pliegue_epicantico', models.PositiveSmallIntegerField(verbose_name='Pliegue epicántico')),
                ('color_ojos', models.PositiveSmallIntegerField(verbose_name='Color de ojos')),
                ('base_nariz', models.PositiveSmallIntegerField(verbose_name='Base de la nariz')),
                ('grosor_labios', models.PositiveSmallIntegerField(verbose_name='Grosor de labios')),
                ('comisura_labios', models.PositiveSmallIntegerField(verbose_name='Comisura de labios')),
                ('pilosidad_facial', models.PositiveSmallIntegerField(verbose_name='Pilosidad facial')),
                ('nariz_perfil', models.PositiveSmallIntegerField(verbose_name='Nariz de perfil')),
                ('proyeccion_labios', models.PositiveSmallIntegerField(verbose_name='Proyección de labios (perfil)')),
                ('forma_orejas', models.PositiveSmallIntegerField(verbose_name='Forma de orejas')),
                ('persona', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='phototag.persona')),
            ],
        ),
    ]
