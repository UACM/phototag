# Generated by Django 4.1.7 on 2023-05-05 19:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phototag', '0007_alter_rasgo_base_nariz_alter_rasgo_color_ojos_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='persona',
            name='notas',
            field=models.TextField(blank=True, null=True),
        ),
    ]
