from django.db import models
from django.contrib.auth.models import User
from django.utils.html import mark_safe
from django.conf import settings
from .landmarks import landmarks_df
from pathlib import Path
import pickle
from django.core.files import File


class Persona(models.Model):
    nombre = models.CharField(max_length=255,
                              blank=True,
                              null=True)
    sexo = models.CharField(max_length=1,
                            choices=(('M', 'Masculino'),
                                     ('F', 'Femenino')))

    notas = models.TextField(blank=True,
                             null=True)

    banco_publico = models.BooleanField(default=False)
    
    autor = models.ForeignKey(User,
                              on_delete=models.CASCADE,
                              null=True, blank=True)

    def __str__(self):
        if self.nombre:
            return self.nombre
        else:
            return "persona %s" % self.id



class Relacion(models.Model):
    ego = models.ForeignKey(Persona, on_delete=models.CASCADE)
    alter = models.ForeignKey(Persona, related_name='parientes', on_delete=models.CASCADE)
    distancia = models.FloatField()

    autor = models.ForeignKey(User,
                              on_delete=models.CASCADE,
                              null=True, blank=True)

    def __str__(self):
        return "%s ↤ %s ↦ %s" % (self.ego, self.distancia, self.alter)

    class Meta:
        verbose_name_plural = "Relaciones familiares"



class Rasgo(models.Model):
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE, )
    edad = models.PositiveSmallIntegerField()

    nacimiento_cabello = models.PositiveSmallIntegerField(
        "Nacimiento del cabello", null=True, blank=True)
    tipo_cabello = models.PositiveSmallIntegerField(
        "Tipo de cabello", null=True, blank=True)
    contorno_rostro = models.PositiveSmallIntegerField(
        "Contorno del rostro", null=True, blank=True)
    forma_menton = models.PositiveSmallIntegerField(
        "forma del mentón", null=True, blank=True)
    proyeccion_frente = models.PositiveSmallIntegerField(
        "proyección de la frente", null=True, blank=True)
    altura_frente = models.PositiveSmallIntegerField(
        "altura de la frente", null=True, blank=True)
    pilosidad_cejas = models.PositiveSmallIntegerField(
        "Pilosidad de cejas", null=True, blank=True)
    forma_cejas = models.PositiveSmallIntegerField(
        "forma de las cejas", null=True, blank=True)
    linealidad_ojos = models.PositiveSmallIntegerField(
        "Linealidad de los ojos", null=True, blank=True)
    forma_ojos = models.PositiveSmallIntegerField(
        "Forma de los ojos", null=True, blank=True)
    pliegue_epicantico = models.PositiveSmallIntegerField(
        "Pliegue epicántico", null=True, blank=True)
    color_ojos = models.PositiveSmallIntegerField(
        "Color de ojos", null=True, blank=True)
    base_nariz = models.PositiveSmallIntegerField(
        "Base de la nariz", null=True, blank=True)
    punta_nariz = models.PositiveSmallIntegerField(
        "punta de la nariz", null=True, blank=True)
    nariz_perfil = models.PositiveSmallIntegerField(
        "Nariz de perfil", null=True, blank=True)    
    grosor_labios = models.PositiveSmallIntegerField(
        "Grosor de labios", null=True, blank=True)
    comisura_labios = models.PositiveSmallIntegerField(
        "Comisura de labios", null=True, blank=True)
    simetria_labios = models.PositiveSmallIntegerField(
        "simetría de labios", null=True, blank=True)
    tamano_boca = models.PositiveSmallIntegerField(
        "tamaño de la boca", null=True, blank=True)
    proyeccion_labios = models.PositiveSmallIntegerField(
        "Proyección de labios (perfil)", null=True, blank=True)    
    pilosidad_facial = models.PositiveSmallIntegerField(
        "Pilosidad facial", null=True, blank=True)
    forma_orejas = models.PositiveSmallIntegerField(
        "Forma de orejas", null=True, blank=True)
    lobulo_oreja = models.PositiveSmallIntegerField(
        "Lóbulo de la oreja", null=True, blank=True)

    

    autor = models.ForeignKey(User,
                              on_delete=models.CASCADE,
                              null=True, blank=True)

    class Meta:
        unique_together = ('persona', 'edad',)

    def __str__(self):
        return "rasgos de %s a %s años" % (self.persona.nombre,
                                           self.edad)



class Foto(models.Model):
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE, )
    archivo = models.CharField(max_length=640)
    foto = models.ImageField(null=True,
                             blank=True)

    def img_tag(self):
        return mark_safe(f'<img src="{self.foto.url}" width="150" />')
    
    img_tag.short_description = 'Image'
    
    nube_puntos = models.FileField(null=True,
                                   blank=True)
    
    edad = models.PositiveSmallIntegerField()


    autor = models.ForeignKey(User,
                              on_delete=models.CASCADE,
                              null=True, blank=True)


    def detecta_landmarks(self):
        if not self.foto:
            return False
        
        pickle_path = Path(Path(self.foto.path).parent,
                          Path(self.foto.path).stem + '.pickle')
        
        with open(pickle_path, 'wb') as f:
            df = landmarks_df(self.foto.path)
            pickle.dump(df, f)

        with pickle_path.open(mode="rb") as f:
            self.nube_puntos = File(f, name=pickle_path.name)
            self.save()

            
    def __str__(self):
        return "foto %s de %s" % (self.id, self.persona.nombre)



# for photo in photos:
#     print(landmarks_df(photo))

# photos = glob.glob('data/FGNET/images/%s*JPG' % args.subject)

# fig = plt.figure(figsize = (15, 15))
# ax = plt.axes(projection ="3d")
# angle = 90
# ax.view_init(90, angle)
# color = iter(cm.rainbow(np.linspace(0, 1, len(photos))))
        
