# coding: utf-8
from django.core.management.base import BaseCommand
import argparse
from phototag.models import Foto

class Command(BaseCommand):
    help = 'Computa nube de puntos para cada foto'

    def add_arguments(self, parser):
        parser.add_argument('id',
                            help='ID de la foto, o "lista" para devolver todos los id')
    
    def handle(self, *args, **options):
        nubes(options['id'])


def nubes(foto_id):
    if foto_id == "lista":
        for f in Foto.objects.all():
            print(f.id)
    else:
        f = Foto.objects.get(pk=int(foto_id))
        f.detecta_landmarks()

