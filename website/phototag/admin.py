from django.contrib import admin
from .models import Persona, Relacion, Foto, Rasgo
from reversion.admin import VersionAdmin
from django.contrib.auth.models import User


class AutoAutor:

    def save_model(self, request, obj, form, change):
        obj.autor = request.user
        super().save_model(request, obj, form, change)

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.autor = request.user
            instance.save()
        formset.save_m2m()


class FotoInline(admin.TabularInline):
    model = Foto
    extra = 1
    readonly_fields = ['img_tag', ]    
    exclude = ['autor', ]
    


class RasgoInline(admin.TabularInline):
    model = Rasgo
    extra = 0
    exclude = ['autor', ]


class RelacionInline(admin.StackedInline):
    model = Relacion
    extra = 0
    fk_name = 'ego'
    search_fields = ['alter', ]
    exclude = ['autor', ]


    
@admin.action(description="Genera nubes de puntos para todas las fotos las personas seleccionadas")
def nubes_de_puntos(modeladmin, request, queryset):
    for p in queryset:
        for f in p.foto_set.all():
            f.detecta_landmarks()

        
    
@admin.register(Persona)
class PersonaAdmin(AutoAutor, VersionAdmin):
    list_display = ['nombre',
                    'sexo',
                    'autor']
    list_filter = ['sexo', 'autor',]
    search_fields = ['nombre', ]
    readonly_fields = ['autor', ]
    inlines = [FotoInline,
               RasgoInline,
               RelacionInline]
    actions = [nubes_de_puntos, ]


    
@admin.register(Rasgo)
class RasgoAdmin(AutoAutor, VersionAdmin):
    list_display = ['persona',
                    'edad',
                    'autor']

    list_filter = ['autor',]
    readonly_fields = ['autor', ]


@admin.register(Relacion)
class RelacionAdmin(AutoAutor, VersionAdmin):
    list_display = ['ego',
                    'distancia',
                    'alter',
                    'autor']

    autocomplete_fields = ['ego', 'alter', ]
    readonly_fields = ['autor', ]
    list_filter = ['autor',]
