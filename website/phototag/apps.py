from django.apps import AppConfig


class PhototagConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'phototag'
